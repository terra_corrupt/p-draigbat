﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatCat2
{
    class World : IEntity
    {

        public override void Update(IEntity e, IEntity f)
        {
            e.Response();
            f.Response();
        }

        public String Response()
        {
            return "Hello World";
        }
  
    }
}
