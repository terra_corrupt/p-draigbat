﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatCat2
{
    public interface IEntity
    {

        void Update(IEntity e);
        void Update(IEntity _t1, IEntity _p1);
        String Response();

        
    }

}
